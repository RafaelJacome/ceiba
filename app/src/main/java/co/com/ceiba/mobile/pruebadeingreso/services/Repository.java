package co.com.ceiba.mobile.pruebadeingreso.services;

import java.io.IOException;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.models.User;
import co.com.ceiba.mobile.pruebadeingreso.models.UserPost;
import retrofit2.Call;
import retrofit2.Response;

public class Repository {
    private IEndpoints iEndpoints;

    public Repository() {
        Endpoints endpoints = new Endpoints();
        this.iEndpoints = (IEndpoints) endpoints.getInstanceService(IEndpoints.class);
    }

    private IOException defaultError(String message) {
        return new IOException(message);
    }

    public List<User> getUsers() throws IOException {
        try {
            Call<List<User>> call = iEndpoints.getUsers();
            Response<List<User>> response = call.execute();
            if (response.errorBody() != null) {
                throw defaultError(response.errorBody().string());
            } else {
                return response.body();
            }
        } catch (IOException e) {
            throw defaultError(e.getMessage());
        }
    }

    public List<UserPost> getUserPosts(String userId) throws IOException {
        try {
            Call<List<UserPost>> call = iEndpoints.getUserPosts(userId);
            Response<List<UserPost>> response = call.execute();
            if (response.errorBody() != null) {
                throw defaultError(response.errorBody().string());
            } else {
                return response.body();
            }
        } catch (IOException e) {
            throw defaultError(e.getMessage());
        }
    }
}
