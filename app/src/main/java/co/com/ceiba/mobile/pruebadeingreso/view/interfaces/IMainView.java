package co.com.ceiba.mobile.pruebadeingreso.view.interfaces;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.models.User;

public interface IMainView extends IBaseView {
    void showUsers(List<User> users);

    void intentToViewUserPost(User user);

    void listIsEmpty(boolean isEmpty);
}
