package co.com.ceiba.mobile.pruebadeingreso.helper;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import co.com.ceiba.mobile.pruebadeingreso.dao.UserDao;
import co.com.ceiba.mobile.pruebadeingreso.dao.UserPostDao;

import static co.com.ceiba.mobile.pruebadeingreso.helper.Constants.DATABASE_NAME;
import static co.com.ceiba.mobile.pruebadeingreso.helper.Constants.DATABASE_VERSION;

public class Database {

    private Context context;
    private DatabaseHelper dbHelper;
    public static UserDao userDao;
    public static UserPostDao userPostDao;

    public Database(Context context) {
        this.context = context;
    }

    public Database open() {
        try {
            dbHelper = new DatabaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
            SQLiteDatabase sdb = dbHelper.getWritableDatabase();
            userDao = new UserDao(sdb);
            userPostDao = new UserPostDao(sdb);
        } catch (SQLException ex) {
            throw ex;
        }
        return this;
    }

    public void close() {
        dbHelper.close();
    }
}
