package co.com.ceiba.mobile.pruebadeingreso.view.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.models.User;
import co.com.ceiba.mobile.pruebadeingreso.view.interfaces.IMainView;

public class AdapterUsers extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private List<User> users;
    private List<User> usersFull;
    private IMainView iMainView;

    public AdapterUsers(List<User> users, IMainView iMainView) {
        this.users = users;
        this.usersFull = new ArrayList<>(users);
        this.iMainView = iMainView;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CustomViewHolder customViewHolder = (CustomViewHolder) holder;
        final User user = users.get(position);
        customViewHolder.getTvName().setText(user.getName());
        customViewHolder.getTvPhone().setText(user.getPhone());
        customViewHolder.getTvEmail().setText(user.getEmail());
        customViewHolder.getBtnViewpost().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMainView.intentToViewUserPost(user);
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public Filter getFilter() {
        return userFilter;
    }

    private Filter userFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<User> filteredUsers = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredUsers.addAll(usersFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (User user : usersFull) {
                    if (user.getName().toLowerCase().contains(filterPattern)) {
                        filteredUsers.add(user);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredUsers;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            users.clear();
            users.addAll((List) results.values);
            notifyDataSetChanged();
            iMainView.listIsEmpty(users.size() == 0);
        }
    };
}
