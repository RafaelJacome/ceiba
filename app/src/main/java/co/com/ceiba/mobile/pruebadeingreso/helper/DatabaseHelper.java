package co.com.ceiba.mobile.pruebadeingreso.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import co.com.ceiba.mobile.pruebadeingreso.schemes.IUserPostScheme;
import co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme;

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        createTables(db);
    }

    public void createTables(SQLiteDatabase db) {
        db.execSQL(IUserScheme.USERS_TABLE_CREATE);
        db.execSQL(IUserPostScheme.USERPOSTS_TABLE_CREATE);
    }

    public void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + IUserScheme.USER_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + IUserPostScheme.USERPOST_TABLE);
    }
}
