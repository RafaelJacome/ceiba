package co.com.ceiba.mobile.pruebadeingreso.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.models.User;
import co.com.ceiba.mobile.pruebadeingreso.presenter.MainPresenter;
import co.com.ceiba.mobile.pruebadeingreso.view.adapters.AdapterUsers;
import co.com.ceiba.mobile.pruebadeingreso.view.interfaces.IMainView;

import static co.com.ceiba.mobile.pruebadeingreso.helper.Constants.USER;

public class MainActivity extends BaseActivity<MainPresenter> implements IMainView {
    private AdapterUsers adapterUsers;
    private EditText etSearch;
    private RecyclerView recyclerView;
    private View layoutEmptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initListener();
        initPresenter();
        getUsers();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initView() {
        etSearch = findViewById(R.id.editTextSearch);
        recyclerView = findViewById(R.id.recyclerViewSearchResults);
        layoutEmptyView = findViewById(R.id.layout_empty_view);
    }

    private void initPresenter() {
        setPresenter(new MainPresenter());
        getPresenter().inject(this, getValidateInternet());
    }

    private void initListener() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapterUsers != null) {
                    adapterUsers.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void getUsers() {
        initProgressDialog(false, getString(R.string.espere_por_favor), this);
        getPresenter().getUsersDb(getApplicationContext());
    }

    @Override
    public void showUsers(final List<User> users) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissProgressDialog();
                adapterUsers = new AdapterUsers(users, MainActivity.this);
                LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapterUsers);
            }
        });
    }

    @Override
    public void intentToViewUserPost(User user) {
        Intent intent = new Intent(this, PostActivity.class);
        intent.putExtra(USER, user);
        startActivity(intent);
    }

    @Override
    public void listIsEmpty(boolean isEmpty) {
        if (isEmpty) {
            layoutEmptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            layoutEmptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showListEmpty(String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissProgressDialog();
                listIsEmpty(true);
            }
        });
    }
}