package co.com.ceiba.mobile.pruebadeingreso.presenter;

import co.com.ceiba.mobile.pruebadeingreso.helper.ValidateInternet;
import co.com.ceiba.mobile.pruebadeingreso.view.interfaces.IBaseView;

public class BasePresenter<T extends IBaseView> {
    private ValidateInternet validateInternet;
    private T view;

    public void inject(T view, ValidateInternet validateInternet) {
        this.validateInternet = validateInternet;
        this.view = view;
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }

    public T getView() {
        return view;
    }

}
