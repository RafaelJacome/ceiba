package co.com.ceiba.mobile.pruebadeingreso.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.helper.DatabaseHelper;
import co.com.ceiba.mobile.pruebadeingreso.models.Address;
import co.com.ceiba.mobile.pruebadeingreso.models.Company;
import co.com.ceiba.mobile.pruebadeingreso.models.Geo;
import co.com.ceiba.mobile.pruebadeingreso.models.User;
import co.com.ceiba.mobile.pruebadeingreso.provider.DbContentProvider;

import static co.com.ceiba.mobile.pruebadeingreso.helper.Constants.DATABASE_NAME;
import static co.com.ceiba.mobile.pruebadeingreso.helper.Constants.DATABASE_VERSION;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_ADDRESS_CITY;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_ADDRESS_STREET;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_ADDRESS_SUITE;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_ADDRESS_ZIPCODE;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_COMPANY_BS;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_COMPANY_CATCH_PHRASE;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_COMPANY_NAME;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_EMAIL;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_GEO_LAT;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_GEO_LNG;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_ID;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_NAME;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_PHONE;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_USERNAME;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.COLUMN_WEBSITE;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.USERS_TABLE_SELECT;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserScheme.USER_TABLE;

public class UserDao extends DbContentProvider {

    private static final String TAG = "UserDao";
    private Cursor cursor;
    private ContentValues contentValues;

    public UserDao(SQLiteDatabase mDb) {
        super(mDb);
    }


    public Boolean saveUser(User user) {
        setContentValuesSaveUser(user);
        try {
            return insert(USER_TABLE, getContentValues()) >= 0;
        } catch (SQLiteException ex) {
            Log.i(TAG, ex.getMessage());
            return false;
        }
    }

    private void setContentValuesSaveUser(User user) {
        contentValues = new ContentValues();
        contentValues.put(COLUMN_ID, user.getId());
        contentValues.put(COLUMN_NAME, user.getName());
        contentValues.put(COLUMN_USERNAME, user.getUsername());
        contentValues.put(COLUMN_EMAIL, user.getEmail());
        contentValues.put(COLUMN_PHONE, user.getPhone());
        contentValues.put(COLUMN_WEBSITE, user.getWebsite());
        contentValues.put(COLUMN_ADDRESS_STREET, user.getAddress().getStreet());
        contentValues.put(COLUMN_ADDRESS_CITY, user.getAddress().getCity());
        contentValues.put(COLUMN_ADDRESS_SUITE, user.getAddress().getSuite());
        contentValues.put(COLUMN_ADDRESS_ZIPCODE, user.getAddress().getZipcode());
        contentValues.put(COLUMN_GEO_LAT, user.getAddress().getGeo().getLat());
        contentValues.put(COLUMN_GEO_LNG, user.getAddress().getGeo().getLng());
        contentValues.put(COLUMN_COMPANY_NAME, user.getCompany().getName());
        contentValues.put(COLUMN_COMPANY_CATCH_PHRASE, user.getCompany().getCatchPhrase());
        contentValues.put(COLUMN_COMPANY_BS, user.getCompany().getBs());
    }

    private ContentValues getContentValues() {
        return contentValues;
    }

    public List<User> getUsers(Context context) {
        DatabaseHelper conn = new DatabaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase sdb = conn.getReadableDatabase();
        List<User> users = new ArrayList<>();
        try {
            cursor = sdb.rawQuery(USERS_TABLE_SELECT, null);
            while (cursor.moveToNext()) {
                User user = new User();
                Address address = new Address();
                Geo geo = new Geo();
                Company company = new Company();
                int i = 0;
                user.setId(cursor.getString(i++));
                user.setName(cursor.getString(i++));
                user.setUsername(cursor.getString(i++));
                user.setEmail(cursor.getString(i++));
                user.setPhone(cursor.getString(i++));
                user.setWebsite(cursor.getString(i++));
                user.setAddress(address);
                user.getAddress().setStreet(cursor.getString(i++));
                user.getAddress().setSuite(cursor.getString(i++));
                user.getAddress().setCity(cursor.getString(i++));
                user.getAddress().setZipcode(cursor.getString(i++));
                user.getAddress().setGeo(geo);
                user.getAddress().getGeo().setLat(cursor.getString(i++));
                user.getAddress().getGeo().setLng(cursor.getString(i++));
                user.setCompany(company);
                user.getCompany().setName(cursor.getString(i++));
                user.getCompany().setCatchPhrase(cursor.getString(i++));
                user.getCompany().setBs(cursor.getString(i));
                users.add(user);
            }
        } catch (SQLiteException ex) {
            Log.i(TAG, ex.getMessage());
            return new ArrayList<>();
        } finally {
            cursor.close();
            sdb.close();
            conn.close();
        }
        return users;
    }
}
