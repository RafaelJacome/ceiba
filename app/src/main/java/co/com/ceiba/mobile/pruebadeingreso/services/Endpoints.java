package co.com.ceiba.mobile.pruebadeingreso.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Endpoints {
    public static final String URL_BASE = "https://jsonplaceholder.typicode.com";
    public static final String GET_USERS = "/users";
    public static final String GET_POST_USER = "/posts?";
    private Retrofit retrofit;

    public Endpoints() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS).build();
        retrofit = new Retrofit.Builder().baseUrl(URL_BASE)
                .client(okHttpClient)
                .addConverterFactory(getGsonConverter())
                .build();
    }

    private Converter.Factory getGsonConverter() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        return GsonConverterFactory.create(gson);
    }

    public Object getInstanceService(Class service) {
        return retrofit.create(service);
    }
}
