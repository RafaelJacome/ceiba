package co.com.ceiba.mobile.pruebadeingreso.services;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.models.User;
import co.com.ceiba.mobile.pruebadeingreso.models.UserPost;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static co.com.ceiba.mobile.pruebadeingreso.services.Endpoints.GET_POST_USER;
import static co.com.ceiba.mobile.pruebadeingreso.services.Endpoints.GET_USERS;

public interface IEndpoints {
    @GET(GET_USERS)
    Call<List<User>> getUsers();

    @GET(GET_POST_USER)
    Call<List<UserPost>> getUserPosts(@Query("userId") String userId);
}
