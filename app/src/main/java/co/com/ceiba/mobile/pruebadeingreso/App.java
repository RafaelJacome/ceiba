package co.com.ceiba.mobile.pruebadeingreso;

import android.app.Application;
import android.content.IntentFilter;

import co.com.ceiba.mobile.pruebadeingreso.helper.Database;
import io.realm.internal.network.NetworkStateReceiver;

public class App extends Application {
    public static Database mDb;
    private final NetworkStateReceiver NETWORK_STATE_RECEIVER = new NetworkStateReceiver();

    @Override
    public void onCreate() {
        super.onCreate();
        createDatabase();
        registerNetworkStateReceiver();
    }

    private void createDatabase() {
        mDb = new Database(this);
        mDb.open();
    }

    private void registerNetworkStateReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(NETWORK_STATE_RECEIVER, intentFilter);
    }

    @Override
    public void onTerminate() {
        mDb.close();
        super.onTerminate();
    }
}
