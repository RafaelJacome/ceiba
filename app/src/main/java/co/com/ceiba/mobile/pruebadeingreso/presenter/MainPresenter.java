package co.com.ceiba.mobile.pruebadeingreso.presenter;

import android.content.Context;

import java.io.IOException;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.helper.Database;
import co.com.ceiba.mobile.pruebadeingreso.models.User;
import co.com.ceiba.mobile.pruebadeingreso.services.Repository;
import co.com.ceiba.mobile.pruebadeingreso.view.interfaces.IMainView;

public class MainPresenter extends BasePresenter<IMainView> {
    private Repository repository;

    public void getUsers(Context context) {
        if (getValidateInternet().isConnected()) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        repository = new Repository();
                        List<User> users = repository.getUsers();
                        saveUsersDb(users);
                        getView().showUsers(users);
                    } catch (IOException e) {
                        e.printStackTrace();
                        getView().showListEmpty(e.getMessage());
                    }
                }
            });
            thread.start();
        } else {
            context.getString(R.string.title_validate_internet);
            getView().showAlertDialog(context.getString(R.string.title_validate_internet), context.getString(R.string.message_validate_internet), context);
        }
    }

    private void saveUsersDb(final List<User> users) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                createUsersLocalDb(users);
            }
        });
        thread.start();
    }

    private void createUsersLocalDb(List<User> users) {
        for (User user : users) {
            Database.userDao.saveUser(user);
        }
    }

    public void getUsersDb(Context context) {
        List<User> users = Database.userDao.getUsers(context);
        if (users == null || users.isEmpty()) {
            getUsers(context);
        } else {
            getView().showUsers(users);
        }
    }
}
