package co.com.ceiba.mobile.pruebadeingreso.helper;

public class Constants {
    public static final String USER = "user";
    public static final String DATABASE_NAME = "db_users";
    public static final int DATABASE_VERSION = 1;
}
