package co.com.ceiba.mobile.pruebadeingreso.presenter;

import android.content.Context;

import java.io.IOException;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.helper.Database;
import co.com.ceiba.mobile.pruebadeingreso.models.UserPost;
import co.com.ceiba.mobile.pruebadeingreso.services.Repository;
import co.com.ceiba.mobile.pruebadeingreso.view.interfaces.IPostView;

public class PostPresenter extends BasePresenter<IPostView> {

    private Repository repository;

    public void getPostUser(Context context, final String userId) {
        if (getValidateInternet().isConnected()) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        repository = new Repository();
                        List<UserPost> userPosts = repository.getUserPosts(userId);
                        saveUserPostsDb(userPosts, userId);
                        getView().showUserPosts(userPosts);
                    } catch (IOException e) {
                        e.printStackTrace();
                        getView().showListEmpty(e.getMessage());
                    }
                }
            });
            thread.start();
        } else {
            context.getString(R.string.title_validate_internet);
            getView().showAlertDialog(context.getString(R.string.title_validate_internet), context.getString(R.string.message_validate_internet), context);
        }
    }

    private void saveUserPostsDb(final List<UserPost> userPosts, final String userId) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                createUserPostsLocalDb(userPosts, userId);
            }
        });
        thread.start();
    }

    private void createUserPostsLocalDb(List<UserPost> userPosts, String userId) {
        for (UserPost userPost : userPosts) {
            Database.userPostDao.saveUserPost(userPost, userId);
        }
    }

    public void getUserPostsDb(Context context, String userId) {
        List<UserPost> userPosts = Database.userPostDao.getUserPosts(context, userId);
        if (userPosts == null || userPosts.isEmpty()) {
            getPostUser(context, userId);
        } else {
            getView().showUserPosts(userPosts);
        }
    }
}
