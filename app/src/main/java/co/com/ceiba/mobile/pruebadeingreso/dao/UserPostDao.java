package co.com.ceiba.mobile.pruebadeingreso.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.helper.DatabaseHelper;
import co.com.ceiba.mobile.pruebadeingreso.models.UserPost;
import co.com.ceiba.mobile.pruebadeingreso.provider.DbContentProvider;

import static co.com.ceiba.mobile.pruebadeingreso.helper.Constants.DATABASE_NAME;
import static co.com.ceiba.mobile.pruebadeingreso.helper.Constants.DATABASE_VERSION;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserPostScheme.COLUMN_BODY;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserPostScheme.COLUMN_ID;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserPostScheme.COLUMN_TITLE;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserPostScheme.COLUMN_USER_ID;
import static co.com.ceiba.mobile.pruebadeingreso.schemes.IUserPostScheme.USERPOST_TABLE;

public class UserPostDao extends DbContentProvider {

    private static final String TAG = "UserPostDao";
    private Cursor cursor;
    private ContentValues contentValues;

    public UserPostDao(SQLiteDatabase mDb) {
        super(mDb);
    }

    public List<UserPost> getUserPosts(Context context, String userId) {
        DatabaseHelper conn = new DatabaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase sdb = conn.getReadableDatabase();
        List<UserPost> userPosts = new ArrayList<>();
        String[] parameters = {userId};
        String[] fields = {COLUMN_ID, COLUMN_TITLE, COLUMN_BODY};
        try {
            cursor = sdb.query(USERPOST_TABLE, fields, COLUMN_USER_ID + " = ?", parameters, null, null, null);
            while (cursor.moveToNext()) {
                UserPost userPost = new UserPost();
                int i = 0;
                userPost.setId(cursor.getString(i++));
                userPost.setTitle(cursor.getString(i++));
                userPost.setBody(cursor.getString(i));
                userPosts.add(userPost);
            }
        } catch (SQLiteException ex) {
            Log.i(TAG, ex.getMessage());
            return new ArrayList<>();
        } finally {
            cursor.close();
            sdb.close();
            conn.close();
        }
        return userPosts;
    }

    public Boolean saveUserPost(UserPost userPost, String userId) {
        setContentValuesSaveUserPost(userPost, userId);
        try {
            return insert(USERPOST_TABLE, getContentValues()) >= 0;
        } catch (SQLiteException ex) {
            Log.i(TAG, ex.getMessage());
            return false;
        }
    }

    private void setContentValuesSaveUserPost(UserPost userPost, String userId) {
        contentValues = new ContentValues();
        contentValues.put(COLUMN_ID, userPost.getId());
        contentValues.put(COLUMN_USER_ID, userId);
        contentValues.put(COLUMN_TITLE, userPost.getTitle());
        contentValues.put(COLUMN_BODY, userPost.getBody());
    }

    private ContentValues getContentValues() {
        return contentValues;
    }
}
