package co.com.ceiba.mobile.pruebadeingreso.view.interfaces;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.models.UserPost;

public interface IPostView extends IBaseView {
    void showUserPosts(List<UserPost> userPosts);
}
