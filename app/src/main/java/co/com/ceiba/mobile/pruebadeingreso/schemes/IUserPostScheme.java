package co.com.ceiba.mobile.pruebadeingreso.schemes;

public interface IUserPostScheme {

    String USERPOST_TABLE = "userposts";
    String COLUMN_ID = "id";
    String COLUMN_USER_ID = "id_user";
    String COLUMN_TITLE = "title";
    String COLUMN_BODY = "body";

    String USERPOSTS_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " +
            USERPOST_TABLE + " ( " +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            COLUMN_USER_ID + " TEXT NOT NULL, " +
            COLUMN_TITLE + " TEXT, " +
            COLUMN_BODY + " TEXT " +
            " );";
}
