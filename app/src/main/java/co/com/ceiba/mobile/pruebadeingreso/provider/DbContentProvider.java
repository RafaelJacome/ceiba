package co.com.ceiba.mobile.pruebadeingreso.provider;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

public abstract class DbContentProvider {

    public SQLiteDatabase mDb;

    public DbContentProvider(SQLiteDatabase mDb) {
        this.mDb = mDb;
    }

    public long insert(String tableName, ContentValues contentValues) {
        return mDb.insert(tableName, null, contentValues);
    }
}
