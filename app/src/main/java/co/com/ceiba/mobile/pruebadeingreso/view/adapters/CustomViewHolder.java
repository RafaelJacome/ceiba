package co.com.ceiba.mobile.pruebadeingreso.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import co.com.ceiba.mobile.pruebadeingreso.R;

public class CustomViewHolder extends RecyclerView.ViewHolder {
    private TextView tvName;
    private TextView tvPhone;
    private TextView tvEmail;
    private Button btnViewpost;
    private TextView tvTitle;
    private TextView tvBody;

    public TextView getTvName() {
        return tvName;
    }

    public TextView getTvPhone() {
        return tvPhone;
    }

    public TextView getTvEmail() {
        return tvEmail;
    }

    public Button getBtnViewpost() {
        return btnViewpost;
    }

    public TextView getTvTitle() {
        return tvTitle;
    }

    public TextView getTvBody() {
        return tvBody;
    }

    public CustomViewHolder(View view) {
        super(view);
        initView(view);
    }

    private void initView(View view) {
        tvName = view.findViewById(R.id.name);
        tvPhone = view.findViewById(R.id.phone);
        tvEmail = view.findViewById(R.id.email);
        btnViewpost = view.findViewById(R.id.btn_view_post);
        tvTitle = view.findViewById(R.id.title);
        tvBody = view.findViewById(R.id.body);
    }
}
