package co.com.ceiba.mobile.pruebadeingreso.view.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.models.User;
import co.com.ceiba.mobile.pruebadeingreso.models.UserPost;
import co.com.ceiba.mobile.pruebadeingreso.presenter.PostPresenter;
import co.com.ceiba.mobile.pruebadeingreso.view.adapters.AdapterUserPosts;
import co.com.ceiba.mobile.pruebadeingreso.view.interfaces.IPostView;

import static co.com.ceiba.mobile.pruebadeingreso.helper.Constants.USER;

public class PostActivity extends BaseActivity<PostPresenter> implements IPostView {
    private LinearLayout llUserPost;
    private TextView tvName;
    private TextView tvPhone;
    private TextView tvEmail;
    private User user;
    private RecyclerView recyclerView;
    private View layoutEmptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        setPresenter(new PostPresenter());
        getPresenter().inject(this, getValidateInternet());
        getExtras();
        initView();
        setView();
        getPostUser();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void getExtras() {
        user = (User) getIntent().getSerializableExtra(USER);
    }

    private void initView() {
        llUserPost = findViewById(R.id.ll_user_post);
        tvName = findViewById(R.id.name);
        tvPhone = findViewById(R.id.phone);
        tvEmail = findViewById(R.id.email);
        recyclerView = findViewById(R.id.recyclerViewPostsResults);
        layoutEmptyView = findViewById(R.id.layout_empty_view);
    }

    private void setView() {
        tvName.setText(user.getName());
        tvPhone.setText(user.getPhone());
        tvEmail.setText(user.getEmail());
    }

    private void getPostUser() {
        initProgressDialog(false, getString(R.string.espere_por_favor), this);
        getPresenter().getUserPostsDb(getApplicationContext(), user.getId());
    }

    @Override
    public void showUserPosts(final List<UserPost> userPosts) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissProgressDialog();
                AdapterUserPosts adapterUserPosts = new AdapterUserPosts(userPosts, PostActivity.this);
                LinearLayoutManager layoutManager = new LinearLayoutManager(PostActivity.this);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapterUserPosts);
            }
        });
    }

    public void listIsEmpty(boolean isEmpty) {
        if (isEmpty) {
            layoutEmptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            layoutEmptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showListEmpty(String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissProgressDialog();
                listIsEmpty(true);
            }
        });
    }

}
