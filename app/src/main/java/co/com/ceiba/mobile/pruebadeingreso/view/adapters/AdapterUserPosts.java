package co.com.ceiba.mobile.pruebadeingreso.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.models.UserPost;
import co.com.ceiba.mobile.pruebadeingreso.view.interfaces.IPostView;

public class AdapterUserPosts extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<UserPost> userPosts;
    private Context context;
    private IPostView iMainView;

    public AdapterUserPosts(List<UserPost> userPosts, IPostView iMainView) {
        this.userPosts = userPosts;
        this.iMainView = iMainView;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_item, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CustomViewHolder customViewHolder = (CustomViewHolder) holder;
        UserPost userPost = userPosts.get(position);
        customViewHolder.getTvTitle().setText(userPost.getTitle());
        customViewHolder.getTvBody().setText(userPost.getBody());
    }

    @Override
    public int getItemCount() {
        return userPosts.size();
    }
}
