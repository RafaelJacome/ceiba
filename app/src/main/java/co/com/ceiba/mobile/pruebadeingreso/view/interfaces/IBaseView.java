package co.com.ceiba.mobile.pruebadeingreso.view.interfaces;

import android.content.Context;

public interface IBaseView {
    void showAlertDialog(String title, String message, Context context);

    void showListEmpty(String message);
}
