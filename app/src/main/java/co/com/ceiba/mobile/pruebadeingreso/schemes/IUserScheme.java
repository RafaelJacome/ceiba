package co.com.ceiba.mobile.pruebadeingreso.schemes;

public interface IUserScheme {
    String USER_TABLE = "users";
    String COLUMN_ID = "id";
    String COLUMN_NAME = "name";
    String COLUMN_USERNAME = "username";
    String COLUMN_EMAIL = "email";
    String COLUMN_PHONE = "phone";
    String COLUMN_WEBSITE = "website";
    String COLUMN_ADDRESS_STREET = "address_street";
    String COLUMN_ADDRESS_SUITE = "address_suite";
    String COLUMN_ADDRESS_CITY = "address_city";
    String COLUMN_ADDRESS_ZIPCODE = "address_zipcode";
    String COLUMN_GEO_LAT = "geo_lat";
    String COLUMN_GEO_LNG = "geo_lng";
    String COLUMN_COMPANY_NAME = "company_name";
    String COLUMN_COMPANY_CATCH_PHRASE = "company_catch_phrase";
    String COLUMN_COMPANY_BS = "company_bs";


    String USERS_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " +
            USER_TABLE + " ( " +
            COLUMN_ID + " INTEGER PRIMARY KEY NOT NULL, " +
            COLUMN_NAME + " TEXT NOT NULL, " +
            COLUMN_USERNAME + " TEXT, " +
            COLUMN_EMAIL + " TEXT, " +
            COLUMN_PHONE + " TEXT, " +
            COLUMN_WEBSITE + " TEXT, " +
            COLUMN_ADDRESS_STREET + " TEXT, " +
            COLUMN_ADDRESS_SUITE + " TEXT, " +
            COLUMN_ADDRESS_CITY + " TEXT, " +
            COLUMN_ADDRESS_ZIPCODE + " TEXT, " +
            COLUMN_GEO_LAT + " TEXT, " +
            COLUMN_GEO_LNG + " TEXT, " +
            COLUMN_COMPANY_NAME + " TEXT, " +
            COLUMN_COMPANY_CATCH_PHRASE + " TEXT, " +
            COLUMN_COMPANY_BS + " TEXT " +
            " );";

    String USERS_TABLE_SELECT = "SELECT * FROM " +
            USER_TABLE +
            ";";
}
